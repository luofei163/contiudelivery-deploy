rpm:
	python setup.py bdist_rpm
	
clean:
	rm -rf build
	rm -rf dist
	rm -rf MANIFEST