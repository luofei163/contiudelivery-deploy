import requests
from requests.auth import HTTPDigestAuth
from urlparse import urljoin
import json
from urllib import quote

from contiudelivery.deploy import common
from contiudelivery.deploy import control


def do_request(method, uri, queries=None, data=None):
    info = common.get_config_info('/etc/contiudelivery/web.conf')
    url = info['api']['ip']
    user = info['api']['user']
    password = info['api']['password']
    url = urljoin(url, uri)
    resp = requests.request(method, url,
                            params=queries,
                            data=data,
                            auth=(user, password),
                            headers={'Accept': 'application/json'}
                            )
    return json.loads(resp.content)  # ['results']


def update_iso_info():
    """
        Update iso info to web
    """
    request_result = do_request('GET', 'api/iso/')['results']
    if len(request_result) > 0:
        iso_info = request_result[0]
        import_info = control.import_iso(iso_info['url'])
        if import_info and import_info['result'] == True:
            do_request('PATCH', 'api/iso/%s/' % iso_info['id'], data={'name': '%s' % import_info['iso_name']})


def update_profiles_info():
    all_distros = control.get_all_distros()
    if len(all_distros) > 0:
        for distro in all_distros:
            do_request('POST', 'api/profile/', data={'name': distro['name']})


def deploy_hosts():
    request_result = do_request('GET', 'api/deploy/')['results']
    if len(request_result) > 0:
        control.deploy_hosts(request_result[0])


def main():
    while True:
        update_iso_info()
        break


if __name__ == '__main__':
    # main()
    deploy_hosts()
