'''
Created on Dec 29, 2015

@author: Luo Fei
'''

import paramiko
import sys
import logging

logger = logging.getLogger("ctdyDeploy")

def ssh_login(*args, **kwargs):
  
    def real_decorator(fn):
#         print args, kwargs
#         ip = kwargs['ip']
#         name =  kwargs['name']
#         passwd =  kwargs['passwd']
        def run(*args, **kwargs):
#             print args, kwargs
            try:
                ip = args[0]
                name = args[1]
                passwd = args[2]
                
                client = paramiko.SSHClient()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                client.connect(ip, 22, username=name, password=passwd, timeout=4)
                cmd = fn(*args, **kwargs)
                logger.info('Run %s on %s' % (cmd, ip))
                stdin, stdout, stderr = client.exec_command(cmd) 
                for std in stdout.readlines():
                    logger.info(std.encode('utf8'))
            except paramiko.AuthenticationException:
                logger.exception("Authentication failed when connecting to %s" % ip)
            except Exception as e:
                logger.exception(e)
            finally:
                if client:
                    client.close()
        return run
    return real_decorator

# @ssh_login(ip='10.1.80.155', name='root', passwd= 'qwer1234')
@ssh_login()
def ssh_run_command(ip, name, passwd, cmd):
    return cmd

    
    
if __name__ == '__main__':
    ip='10.1.80.155'
    name='root'
    passwd= 'qwer1234'
    cmd ='ls -l'
    ssh_run_command(ip, name, passwd ,cmd)
#     reinstall_system('we','ere')