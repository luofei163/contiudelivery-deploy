#!/usr/bin/env python
# coding: utf-8
'''
Created on Dec 26, 2015

@author: Luo Fei
'''
import pyjsonrpc
import ConfigParser
from contiudelivery.deploy import common
from contiudelivery.deploy import control
import logging
logger = logging.getLogger("ctdyDeploy")

class RequestHandler(pyjsonrpc.HttpRequestHandler):

    @pyjsonrpc.rpcmethod
    def deploy(self, params):
        """
            Set iso info
        """
        config_file = '/etc/contiudelivery/deploy.conf'
        result = common.set_iso_info(config_file, params)
        logger.info('Receive info %s' % params)
        control.run(config_file)
        return result, params



if __name__ == '__main__':
    # Threading HTTP-Server
    ip = common.get_cobbler_ip()
    http_server = pyjsonrpc.ThreadingHttpServer(
        server_address = (ip, 8888),
        RequestHandlerClass = RequestHandler
    )
    logger.info("Starting HTTP server ...")
    logger.info("URL: http://%s:8888" % ip)
    http_server.serve_forever()