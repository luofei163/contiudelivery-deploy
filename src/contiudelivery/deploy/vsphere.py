'''
Created on Dec 31, 2015

@author: Luo Fei
'''

from __future__ import print_function

from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim, vmodl

import atexit
import sys
import ssl
import random
import logging
import time

logger = logging.getLogger("ctdyDeploy")


def get_obj(content, vimtype, name):
    """
     Get the vsphere object associated with a given text name
    """
    obj = None
    container = content.viewManager.CreateContainerView(content.rootFolder, vimtype, True)
    for c in container.view:
        if c.name == name:
            obj = c
            break
    return obj


def WaitForTasks(tasks, si):
    """
    Given the service instance si and tasks, it returns after all the
    tasks are complete
    """

    pc = si.content.propertyCollector

    taskList = [str(task) for task in tasks]

    # Create filter
    objSpecs = [vmodl.query.PropertyCollector.ObjectSpec(obj=task)
                for task in tasks]
    propSpec = vmodl.query.PropertyCollector.PropertySpec(type=vim.Task,
                                                          pathSet=[], all=True)
    filterSpec = vmodl.query.PropertyCollector.FilterSpec()
    filterSpec.objectSet = objSpecs
    filterSpec.propSet = [propSpec]
    filter = pc.CreateFilter(filterSpec, True)

    try:
        version, state = None, None

        # Loop looking for updates till the state moves to a completed state.
        while len(taskList):
            update = pc.WaitForUpdates(version)
            for filterSet in update.filterSet:
                for objSet in filterSet.objectSet:
                    task = objSet.obj
                    for change in objSet.changeSet:
                        if change.name == 'info':
                            state = change.val.state
                        elif change.name == 'info.state':
                            state = change.val
                        else:
                            continue

                        if not str(task) in taskList:
                            continue

                        if state == vim.TaskInfo.State.success:
                            # Remove task from taskList
                            taskList.remove(str(task))
                        elif state == vim.TaskInfo.State.error:
                            raise task.info.error
            # Move to next version
            version = update.version
    finally:
        if filter:
            filter.Destroy()


def connect(host, user, password, port):
    # port = 443
    si = None
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    context.verify_mode = ssl.CERT_NONE
    try:
        si = SmartConnect(host=host,
                          user=user,
                          pwd=password,
                          port=int(port),
                          sslContext=context)
    except IOError:
        pass
    if not si:
        logger.error("Cannot connect to specified host using specified user: %s" % user)
        sys.exit()

    atexit.register(Disconnect, si)
    return si


def power_vm(si, vmnames, operate):
    """
        si: connect esxi
        vmnames: vm name list
        operate: on -> poweron, off -> poweroff
    """

    try:

        # Retreive the list of Virtual Machines from the inventory objects
        # under the rootFolder
        content = si.content
        obj_view = content.viewManager.CreateContainerView(content.rootFolder,
                                                           [vim.VirtualMachine],
                                                           True)
        vm_list = obj_view.view
        obj_view.Destroy()

        if operate == 'on':
            # Find the vm and power it on
            tasks = [vm.PowerOn() for vm in vm_list if vm.name in vmnames]
        if operate == 'off':
            tasks = [vm.PowerOff() for vm in vm_list if vm.name in vmnames]
        if operate == 'delete':
            tasks = [vm.Destroy() for vm in vm_list if vm.name in vmnames]

        # [print(dir(vm)) for vm in vmList if vm.name in vmnames]
        # Wait for power on to complete
        WaitForTasks(tasks, si)

        logger.info("Virtual Machine(s) have been powered %s successfully" % operate)
    except vmodl.MethodFault as e:
        logger.exception("Caught vmodl fault : " + e.msg)
    except Exception as e:
        logger.exception("Caught Exception : " + str(e))


def _add_new_scsi_controller_helper(scsi_controller_label, properties, bus_number):
    random_key = random.randint(-1050, -1000)
    adapter_type = properties['type'].strip().lower() if 'type' in properties else None
    bus_sharing = properties['bus_sharing'].strip().lower() if 'bus_sharing' in properties else None

    scsi_spec = vim.vm.device.VirtualDeviceSpec()

    if adapter_type == "lsilogic":
        summary = "LSI Logic"
        scsi_spec.device = vim.vm.device.VirtualLsiLogicController()
    elif adapter_type == "lsilogic_sas":
        summary = "LSI Logic Sas"
        scsi_spec.device = vim.vm.device.VirtualLsiLogicSASController()
    elif adapter_type == "paravirtual":
        summary = "VMware paravirtual SCSI"
        scsi_spec.device = vim.vm.device.ParaVirtualSCSIController()
    else:
        # If type not specified or does not match, show error and return
        if not adapter_type:
            err_msg = "The type of '{0}' has not been specified".format(scsi_controller_label)
        else:
            err_msg = "Cannot create '{0}'. Invalid/unsupported type '{1}'".format(scsi_controller_label, adapter_type)
        logger.exception(err_msg)
        raise Exception(err_msg)

    scsi_spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add

    scsi_spec.device.key = random_key
    scsi_spec.device.busNumber = bus_number
    scsi_spec.device.deviceInfo = vim.Description()
    scsi_spec.device.deviceInfo.label = scsi_controller_label
    scsi_spec.device.deviceInfo.summary = summary

    if bus_sharing == "virtual":
        # Virtual disks can be shared between virtual machines on the same server
        scsi_spec.device.sharedBus = vim.vm.device.VirtualSCSIController.Sharing.virtualSharing

    elif bus_sharing == "physical":
        # Virtual disks can be shared between virtual machines on any server
        scsi_spec.device.sharedBus = vim.vm.device.VirtualSCSIController.Sharing.physicalSharing

    else:
        # Virtual disks cannot be shared between virtual machines
        scsi_spec.device.sharedBus = vim.vm.device.VirtualSCSIController.Sharing.noSharing

    return scsi_spec


def _add_new_hard_disk_helper(vm_name, disk_label, datastore, size_gb, unit_number, controller_key=1000,
                              thin_provision=False):
    """

    :param disk_label:
    :param size_gb:
    :param unit_number:
    :param controller_key:
    :param thin_provision:
    :return:
    """
    random_key = random.randint(3000, 5000)

    size_kb = int(size_gb * 1024.0 * 1024.0)

    disk_label = '[%s] %s/%s.vmdk' % (disk_label, vm_name, vm_name)

    disk_spec = vim.vm.device.VirtualDeviceSpec()
    # disk_spec.fileOperation = vim.vm.device.VirtualDeviceSpec.FileOperation.create
    disk_spec.fileOperation = 'create'
    disk_spec.operation = 'add'  # vim.vm.device.VirtualDeviceSpec.Operation.add

    disk_spec.device = vim.vm.device.VirtualDisk()
    disk_spec.device.key = random_key
    disk_spec.device.deviceInfo = vim.Description()
    disk_spec.device.deviceInfo.label = disk_label
    disk_spec.device.deviceInfo.summary = "{0} GB".format(size_gb)
    disk_spec.device.backing = vim.vm.device.VirtualDisk.FlatVer2BackingInfo()
    disk_spec.device.backing.thinProvisioned = thin_provision
    disk_spec.device.backing.diskMode = 'persistent'
    disk_spec.device.backing.fileName = disk_label
    disk_spec.device.backing.datastore = datastore

    disk_spec.device.backing.split = False
    disk_spec.device.backing.writeThrough = False

    disk_spec.device.backing.backingObjectId = ''

    disk_spec.device.controllerKey = controller_key
    disk_spec.device.unitNumber = unit_number
    disk_spec.device.capacityInKB = size_kb
    return disk_spec


class Vmware:
    def __init__(self, host, name, password, port=443):
        logger.info('Init Vmware login information ip:%s name:%s port:%s' % (host, name, port))
        self.connect = connect(host, name, password, port)
        self.content = self.connect.RetrieveContent()
        self.datastore = self.get_datastore_name()

    def vm_power_off(self, name):

        try:

            obj_view = self.content.viewManager.CreateContainerView(self.content.rootFolder,
                                                                    [vim.VirtualMachine],
                                                                    True)
            vm_list = obj_view.view
            obj_view.Destroy()

            tasks = []
            for vm in vm_list:
                if name in vm.name and vm.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:
                    task = vm.PowerOff()
                    tasks.append(task)

            WaitForTasks([task], self.connect)

            logger.info("Virtual Machine(s) %s have been powered on successfully", name)
        except vmodl.MethodFault as e:
            logger.exception("Caught vmodl fault : " + e.msg)
        except Exception as e:
            logger.exception("Caught Exception : " + str(e))

    def vm_power_on(self, name):

        try:

            obj_view = self.content.viewManager.CreateContainerView(self.content.rootFolder,
                                                                    [vim.VirtualMachine],
                                                                    True)
            vm_list = obj_view.view
            obj_view.Destroy()
            tasks = []
            for vm in vm_list:
                if name in vm.name and vm.runtime.powerState == vim.VirtualMachinePowerState.poweredOff:
                    task = vm.PowerOn()
                    tasks.append(task)

            WaitForTasks([task], self.connect)

            logger.info("Virtual Machine(s) %s have been powered on successfully", name)
        except vmodl.MethodFault as e:
            logger.exception("Caught vmodl fault : " + e.msg)
        except Exception as e:
            logger.exception("Caught Exception : " + str(e))

    def vm_power_delete(self, name):

        try:

            obj_view = self.content.viewManager.CreateContainerView(self.content.rootFolder,
                                                                    [vim.VirtualMachine],
                                                                    True)
            vm_list = obj_view.view
            obj_view.Destroy()
            for vm in vm_list:
                if name in vm.name:
                    if vm.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:
                        task = vm.PowerOff()
                        WaitForTasks([task], self.connect)
                        time.sleep(10)

            WaitForTasks([vm.Destroy()], self.connect)
            time.sleep(10)

            logger.info("Virtual Machine(s) %s have been deleted successfully", name)
        except vmodl.MethodFault as e:
            logger.exception("Caught vmodl fault : " + e.msg)
        except Exception as e:
            logger.exception("Caught Exception : " + str(e))

    def vm_set_default_power_op(self):
        """
        Config vm defalut power option
        :return: DefaultPowerOpInfo
        """
        dp = vim.vm.DefaultPowerOpInfo()
        dp.defaultPowerOffType = 'hard'
        dp.powerOffType = 'hard'
        return dp

    def add_nic(self, mac_address):
        nicspec = vim.vm.device.VirtualDeviceSpec()
        nicspec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
        nicspec.device = vim.vm.device.VirtualVmxnet3()
        nicspec.device.addressType = 'Assigned'
        # nicspec.device.addressType = 'Generated'
        nicspec.device.wakeOnLanEnabled = True
        nicspec.device.macAddress = mac_address  # need start 00:5C
        # nicspec.device.unitNumber = 9
        # nicspec.device.controllerKey = 100
        # nicspec.device.key = 4000

        nicspec.device.backing = vim.vm.device.VirtualEthernetCard.NetworkBackingInfo()
        nicspec.device.backing.network = get_obj(self.content, [vim.Network], 'VM Network')
        nicspec.device.backing.deviceName = 'VM Network'

        nicspec.device.connectable = vim.vm.device.VirtualDevice.ConnectInfo()
        nicspec.device.connectable.startConnected = True
        nicspec.device.connectable.allowGuestControl = True
        return nicspec

    def create_vm(self, name, mac_address, mem=1024, cpu=1, guest_id="rhel6_64Guest", version="vmx-08"):
        """
        Create virtual machine
        :param mac_address:
        :param mem: m
        :param cpu:
        :param version:
        :param guest_id:
        :param name: vm name
        :return:
        """

        datacenter = self.content.rootFolder.childEntity[0]
        vm_folder = datacenter.vmFolder
        hosts = datacenter.hostFolder.childEntity
        resource_pool = hosts[0].resourcePool

        # vm_name = 'VM-' + name
        datastore_path = '[' + self.datastore + '] ' + name

        # bare minimum VM shell, no disks. Feel free to edit
        vmx_file = vim.vm.FileInfo(logDirectory=None,
                                   snapshotDirectory=None,
                                   suspendDirectory=None,
                                   vmPathName=datastore_path)

        # device
        devices = []
        nicspec = self.add_nic(mac_address)
        logger.info('Added nic mac address: %s' % mac_address)
        devices.append(nicspec)

        # "Giving first priority for CDrom Device in boot order"
        # bootOptions = vim.vm.BootOptions(bootOrder=[vim.vm.BootOptions.BootableEthernetDevice(nicspec.device)])

        s1 = _add_new_scsi_controller_helper('scsi test', {'type': 'paravirtual'}, 0)
        devices.append(s1)

        disk_spec = _add_new_hard_disk_helper(name, self.get_datastore_name(), self.get_datastore(), 50, 0,
                                              s1.device.key, thin_provision=True)
        devices.append(disk_spec)

        logger.info('Added hard disk %s.vmdk' % name)

        config = vim.vm.ConfigSpec(
                name=name,
                memoryMB=mem,
                numCPUs=cpu,
                files=vmx_file,
                guestId=guest_id,
                version=version,
                deviceChange=devices,
                powerOpInfo=self.vm_set_default_power_op(),
                # bootOptions=bootOptions
        )

        # print "Creating VM %s" % name
        task = vm_folder.CreateVM_Task(config=config, pool=resource_pool)
        WaitForTasks([task], self.connect)  # Start program
        logger.info('Finished create vm , info %s' % ({'name': name, 'mem': mem, 'cpu': cpu}))

        # vmm = get_obj(self.content, [vim.VirtualMachine], 'wolf1')
        # # configg = vim.vm.ConfigSpec(deviceChange=[disk_spec])
        # # task1 = vmm.ReconfigVM_Task(spec=configg)
        #
        # for dev in vmm.config.hardware.device:
        #     print(dev)
        #     if isinstance(dev, vim.vm.device.VirtualSCSIController):
        #         print(dev)
        #         print('ddd')

    def get_datastore_name(self):
        # content = self.connect.RetrieveContent()
        objview = self.content.viewManager.CreateContainerView(self.content.rootFolder,
                                                               [vim.HostSystem],
                                                               True)
        esxi_hosts = objview.view
        objview.Destroy()

        for esxi_host in esxi_hosts:
            storage_system = esxi_host.configManager.storageSystem
            host_file_sys_vol_mount_info = storage_system.fileSystemVolumeInfo.mountInfo
            for host_mount_info in host_file_sys_vol_mount_info:
                # Extract only VMFS volumes
                if host_mount_info.volume.type == "VMFS":
                    return host_mount_info.volume.name
            return None

    def get_datastore(self):
        """

        :return:
        """

        objview = self.content.viewManager.CreateContainerView(self.content.rootFolder,
                                                               [vim.Datastore],
                                                               True)

        datastores = objview.view
        objview.Destroy()

        for datastore in datastores:
            return datastore
        return None


if __name__ == "__main__":
    host = ''
    user = 'root'
    datastore = 'datastore1'
    password = ''
    mac_address = "00:5C:29:F8:2B:0D"
    port = 34441
    vw = Vmware(host, user, password, port)
    # power_vm(vw.connect, 'wolf1', 'off')
    # power_vm(vw.connect, 'wolf1', 'delete')
    # vw.create_vm('wolf1', mac_address=mac_address, mem=2024, cpu=1, guest_id="rhel6_64Guest", version="vmx-11")



    # vmm = get_obj(vw.content, [vim.VirtualMachine], 'wolf1')
    # configg = vim.vm.ConfigSpec(deviceChange=[disk_spec])
    # task1 = vmm.ReconfigVM_Task(spec=configg)

    # for dev in vmm.config.hardware.device:
    #     print('wowoowo')
    #     print(dev)
    #     print('-----------')
    #     if isinstance(dev, vim.vm.device.VirtualSCSIController):
    #         print(dev)
    #         print('ddd')
