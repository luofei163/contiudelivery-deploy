'''
Created on May 3, 2014

@author: Luo Fei
'''
# import yaml
import os
import subprocess
import sys
import time
import requests
import urllib2
import logging

logger = logging.getLogger("ctdyDeploy")

def exception(func):
    def _deco(*args,**kargs):
        try:
            ret = func(*args,**kargs)
            return ret
        except Exception as ex:
            print ex
            co = sys._getframe().f_back
            print co.f_code.co_filename, co.f_lineno,co.f_code.co_name
            
    return _deco

def run_command(cmd, errorMessage):
    '''Execute cmd'''
    sign = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
#    print cmd
    logger.info('Excute %s' % cmd)
    while sign.poll() == None:
        out = sign.stdout.readline()
#        print out
#         logger.info(out)
    sign.wait()
    if sign.returncode != 0:
        if errorMessage != None:
            pass
#             logger.error(errorMessage)
        if exit == True:
            sys.exit(1)
        return False
    else:
        return True

# @exception
# def get_yaml(url):
#     """
#         Get group infomation from client.yaml
#     """
#     with open(url,'r') as f:
#         ya = yaml.load(f)
#       
#     return ya
# 
# def create_kickstart():
#     pass

@exception
def mount_iso(iso, mounted_root_dir):
#     parent_dir = '/deploy'
    dir = os.path.basename(iso).split('.iso')[0]
    mounted_dir = os.path.join(mounted_root_dir,dir)
    
    
        
    if not os.path.isdir(mounted_dir):
        os.makedirs(mounted_dir)
     
    umount_iso(mounted_dir)
       
    if os.path.ismount(mounted_dir):
        subprocess.Popen('umount %s' % mounted_dir, shell=True)
        time.sleep(2)
    cmd = 'mount -o loop %s %s' % (iso,mounted_dir)
    if run_command(cmd, None):
        return mounted_dir
    else:
        logger.error("Mount cmd failed, %s" % cmd)
        return None
    

def umount_iso(mounted_dir):
    if os.path.ismount(mounted_dir):
        run_command('umount %s' % mounted_dir , None)
        

def download_file(url, local_dir):
    #Check is local file, if local iso return 
    if not url.startswith('http://') and not url.startswith('ftp://') and  os.path.exists(url):
        iso_name = (os.path.basename(url)).split('.iso')[0]
        iso_file = url
        return iso_name , iso_file 
        
    logger.info("Start download file %s" % url)
    local_filename = url.split('/')[-1]
    # NOTE the stream=True parameter
    r = requests.get(url, stream=True)
    local_file = os.path.join(local_dir, local_filename)
    with open(local_file, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                f.flush()
#                 os.fsync(f)
                #f.flush() commented by recommendation from J.F.Sebastian
    logger.info("Download %s successfully." % local_file)
    iso_name = local_filename.split('.iso')[0]
    return iso_name, local_file


def checkisomd5(iso):
    result = run_command('checkisomd5 %s' % iso, None)
    if result is True:
        logger.info("checkisomd5 check ok, %s" % iso)
    else:
        logger.error("checkisomd5 %s failed" % iso)
    return result
        
if __name__ == '__main__':
    # Test download_file function 
    download_file('url')
    
    # Test checkisomd5 function 
    checkisomd5('iso')