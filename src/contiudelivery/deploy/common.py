'''
Created on Dec 25, 2015

@author: Luo Fei
'''
from __future__ import with_statement  
import ConfigParser  
import os
import logging
logger = logging.getLogger("ctdyDeploy")

def get_config_section_info(config_file, section):
    """
        Get config_file section info
        Return dict
    """
    config=ConfigParser.ConfigParser()  
    with open(config_file,'r') as cfg:  
        config.readfp(cfg)
        info = dict(config.items(section))
    return info 


def get_config_info(config_file):
    """
        Get config_file section info
        Return dict
    """
    config=ConfigParser.ConfigParser()  
    with open(config_file,'r') as cfg:  
        config.readfp(cfg)
        info = {s:dict(config.items(s)) for s in config.sections()}
    return info 

def get_cobbler_ip():
    """
        Get cobbler server ip from cobbler.conf
    """
    info = get_config_section_info('/etc/contiudelivery/cobbler.conf', 'cobbler')
    
    return info['ip'] 
    
    
def get_iso_info(config_file):
    info = get_config_info(config_file)
    return info['iso']


def set_iso_info(config_file, params):
    """
        Write iso info
    """
    if params['component'] == 'iso':
#         config_file = '/etc/contiudelivery/deploy.conf'
        config=ConfigParser.ConfigParser()
        with open(config_file,'r') as cfg:
            config.readfp(cfg)
        prfix = 'http://'
        iso_path = params['iso_path']
        http_user = params['http_user']
        http_passwd = params['http_passwd']
        url = prfix + "%s:%s@" % (http_user, http_passwd) + iso_path.split(prfix)[-1]
        config.set('iso', 'url', url)
        config.set('iso', 'http_user',params['http_user'])
        config.set('iso', 'http_passwd',params['http_passwd'])
        
        with open(config_file,'w') as cfg:
            config.write(cfg)

    return True        
        
def get_hosts_info(config_file):
    info = get_config_info(config_file)
    hosts = {s: info[s] for s in info if s != 'iso'}
    return hosts
    
if __name__ == '__main__':
    # Test function get_cobbler_ip
    print get_cobbler_ip()
    
    # Test function get_cobbler_ip
    print get_iso_info()
    
    # Test function get_cobbler_ip
    print get_hosts_info()
    
    # Test function set_iso_info
    params = {u'http_passwd': u'cloud', u'iso_path': u'http://10.1.110.123/release/Release/os/ns6.5-sc2.2/2015/10/2015101502/NeoKylin-Linux-Advanced-Server-6.5-adv-lic-build23-x86_64.iso', u'component': u'iso', u'deploy': u'True', u'http_user': u'cloud'}
    set_iso_info(params)