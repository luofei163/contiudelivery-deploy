'''
Created on Dec 25, 2015

@author: Luo Fei
'''


import logging

def init_log():
    logger = logging.getLogger("ctdyDeploy")
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler("/var/log/ctdyDeploy.log")
    fh.setLevel(logging.DEBUG)
    
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    
    ft = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(ft)
    ch.setFormatter(ft)
    
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger
    
logger = init_log()