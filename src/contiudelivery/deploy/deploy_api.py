'''
Created on May 1, 2014

@author: Luo Fei
'''
from time import sleep
import yaml
import xmlrpclib
import os
import time
import subprocess
from contiudelivery.deploy import common

import logging

logger = logging.getLogger("ctdyDeploy")


# def logger(func):
#     def _deco(*args, **kargs):
#         ret = func(*args,**kargs)
#         return ret
#     return  _deco


# def get_group_info():
#     """
#         Get group infomation from client.yaml
#     """
#     url = 'client.yaml'
#     ya = yaml.load(open(url,'r'))
#     
#     for y in ya:
#     
#         if y.find("master") == 0:
#             master_list = ya[y]
#         if y.find('slave') == 0:
#             slave_list =  ya[y]
# #     print master_list
#     return master_list, slave_list




class cobblerHandle:
    def __init__(self):
        url = 'http://%s/cobbler_api' % common.get_cobbler_ip()
        self.server = xmlrpclib.Server(url);

        self.__token = self.server.login("cobbler", "cobbler")

    def import_image(self, name, arch, file_path):
        """
            Import iso to cobbler
        """
        if name.endswith(arch):
            distro_name = name
        else:
            distro_name = '%s-%s' % (name, arch)
        if len(self.server.find_profile({'name': distro_name})) > 0:
            self.server.remove_profile(distro_name, self.__token)
        if len(self.server.find_distro({'name': distro_name})) > 0:
            self.server.remove_distro(distro_name, self.__token)

        # print self.server.find_distro({'name':'Redhat-x86_641'})
        task_id = self.server.background_import({'path': file_path, 'name': name, 'arch': arch}, self.__token)
        logger.info("Start import image, task_id %s" % task_id)
        return task_id, distro_name

    #         return task_id

    def get_status(self, task_id):
        while True:
            status = self.server.get_task_status(task_id)
            if 'complete' in status:
                msg = "Taks %s finished" % task_id
                logger.info(msg)
                return True
            if 'failed' in status:
                msg = 'Task failed, please check /var/log/cobbler/tasks/%s.log' % task_id
                logger.exception(msg)
                raise Exception(msg)
            else:
                sleep(60)
                #         print  self.server.get_task_status('2014-05-02_191323_import')
                #         print self.server.find_profile({'name':'Redhat-x86_64'})
                #         print self.server.find_distro({'name':'Redhat-x86_641'})

    def add_system(self, host, profile):
        """
            Add system for every host
            host: dict , include host infomation
        """
        logger.info("Start add system %s %s" % (host, profile))
        remote = self.server
        token = self.__token
        system_id = remote.new_system(token)
        remote.modify_system(system_id, "name", '-'.join([profile, host['name']]), token)
        remote.modify_system(system_id, 'modify_interface', {
            "macaddress-eth0": host['mac'],
            "ipaddress-eth0": host['ip'],
        }, token)
        remote.modify_system(system_id, 'kickstart', '/var/lib/cobbler/kickstarts/%s.ks' % host['name'], token)
        remote.modify_system(system_id, "profile", profile, token)
        remote.save_system(system_id, token)
        remote.sync(token)
        logger.info("Finished add system %s %s" % (host, profile))

    def remove_system(self, name):
        """
            Remove system 
        """
        remote = self.server
        token = self.__token
        if len(remote.find_system({'name': name})) > 0:
            self.server.remove_system(name, token)
            remote.sync(token)

    def clean_all(self):
        """
            Clean all info
        """
        remote = self.server
        token = self.__token

        # Clean systems
        systems = remote.get_systems()
        [remote.remove_system(system['name'], token) for system in systems if system.has_key('name')]

        # Clean profiles
        profiles = remote.get_profiles()
        [remote.remove_profile(profile['name'], token) for profile in profiles if profile.has_key('name')]

        # Clean distros
        distros = remote.get_distros()
        [remote.remove_distro(distro['name'], token) for distro in distros if distro.has_key('name')]

        # Unmount /deploy
        for path, dirs, files in os.walk('/deploy'):
            for dir in dirs:
                dir_path = os.path.join(path, dir)
                if os.path.ismount(dir_path):
                    subprocess.Popen('umount %s' % dir_path, shell=True)
                    time.sleep(2)
                os.rmdir(dir_path)

    def get_all_distros(self):
        remote = self.server
        distros = remote.get_distros()
        return distros

    def remove_distro(self, name):
        remote = self.server

        distros = remote.get_distros()
        for distro in distros:
            if distro.has_key('name') and name in distro['name']:
                remote.remove_distro(distro['name'], self.__token)

    def remove_system(self, name):
        remote = self.server

        systems = remote.get_systems()

        for system in systems:
            if system.has_key('name') and name in system['name']:
                remote.remove_system(system['name'], self.__token)


if __name__ == '__main__':
    #     masters , slaves = get_group_info()
    ch = cobblerHandle()
    # ch.clean_all()
    # ch.remove_system('host82_133')
    ch.add_system({'name':'host82_133','mac':'00:0C:29:F8:2B:0D','ip':'10.1.82.133'}, 'nsV7Update2-adv-lic-build04-x86_64')
# if True:
#         profile = "Redhat-x86_64"
#         for host in slaves:
#             if not host.has_key('common'):
#                 ch.remove_system(host['name'])
#                 ch.add_system(host, profile)
#         
#                 print host
#                 print host['name']
#
