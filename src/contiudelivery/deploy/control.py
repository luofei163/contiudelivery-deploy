'''
Created on May 4, 2014

@author: Luo Fei
'''
import os
from contiudelivery.deploy import base
# import yaml
import socket
from contiudelivery.deploy import deploy_api
from contiudelivery.deploy import common
from contiudelivery.deploy import client
from contiudelivery.deploy import vsphere
import sys
import logging
import tempfile

logger = logging.getLogger("ctdyDeploy")


def get_host_name():
    """
        Get host name
    """
    name = os.name
    if name == 'nt':
        hostname = os.getenv('computername')
        return hostname
    elif name == 'posix':
        host = os.popen('echo $HOSTNAME')
        try:
            hostname = host.read()
            return hostname
        finally:
            host.close()
    else:
        raise Exception('Can not get hostname, please check.')


def get_host_ip():
    try:
        csock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        csock.connect(('8.8.8.8', 80))
        (addr, port) = csock.getsockname()
        csock.close()
        return addr
    except socket.error:
        return "127.0.0.1"


def create_kickstart(host_info, profile):
    '''
            Create kickstart config file from template for pxe installation.
    '''

    host_name = host_info['name']
    host_ip = host_info['ip']
    host_netmask = host_info['netmask']
    host_gateway = host_info['gateway']
    kickstart = host_info['kickstart']
    deploy_ip = get_host_ip()

    deploy_hostname = get_host_name()

    newlines = []
    with open(kickstart, 'r') as f:
        for line in f.readlines():
            if 'url --url' in line:
                newline = line.replace(line, 'url --url="http://%s/cobbler/ks_mirror/%s/"\n' % (deploy_ip, profile))
                newlines.append(newline)
            elif 'ETC_HOSTNAME' in line:
                newline = line.replace(line, 'HOSTNAME=%s\n' % host_name)
                newlines.append(newline)
            elif 'ETC_HOSTS' in line:
                #                newline = line.replace(line, '127.0.0.1 %s.com.cn %s localhost\n' % (host_name, host_name))
                newline = line.replace(line, '')
                newlines.append(newline)
                newlines.append('%s %s\n' % (deploy_ip, deploy_hostname))
                newlines.append('%s %s\n' % (host_ip, host_name))
            elif 'ntpdate' in line:
                newline = line.replace(line, 'ntpdate %s\n' % deploy_ip)
                newlines.append(newline)
            elif 'puppet agent' in line:
                newline = line.replace(line, 'puppet agent --server=%s\n' % deploy_hostname)
                newlines.append(newline)
            elif 'IPADDR' in line:
                newline = line.replace(line, 'IPADDR=%s\n' % host_ip)
                newlines.append(newline)
            elif 'NETMASK' in line:
                newline = line.replace(line, 'NETMASK=%s\n' % host_netmask)
                newlines.append(newline)
            elif 'GATEWAY' in line:
                newline = line.replace(line, 'GATEWAY=%s\n' % host_gateway)
                newlines.append(newline)
                #         elif 'wget -b' in line:
                #             newline = line.replace(line, 'wget -b  --ftp-user=ftp --ftp-password=ftp ftp://%s:/pub/wget.sh\n' % self.deploy_ip)
                #             newlines.append(newline)

            else:
                newlines.append(line)

    ksfile = '/var/lib/cobbler/kickstarts/%s.ks' % host_name
    if os.path.isfile(ksfile):
        os.remove(ksfile)
    with open(ksfile, 'w') as fw:
        fw.writelines(newlines)
    return ksfile


def clean():
    ch = deploy_api.cobblerHandle()
    ch.clean_all()


def client_reinstall_ssh(host_info):
    cmds = ['yum install -y koan', 'koan -r -s %s' % get_host_ip(), 'reboot']
    for cmd in cmds:
        client.ssh_run_command(host_info['ip'], host_info['user'], host_info['passwd'], cmd)


def client_reinstall_vmware(host_info):
    vv = vsphere.Vmware(host_info['vmware_ip'], host_info['vmware_user'], host_info['vmware_passwd'])
    host_name = host_info['name']
    # vsphere.power_vm(vv.connect, host_name, 'off')
    # vsphere.power_vm(vv.connect, host_name, 'delete')
    vv.vm_power_delete(host_name)
    vv.create_vm(host_name, mac_address=host_info['mac'], mem=2024, cpu=1, guest_id="rhel6_64Guest", version="vmx-11")


def run(config_file):
    ch = deploy_api.cobblerHandle()
    iso_info = common.get_iso_info(config_file)
    hosts_info = common.get_hosts_info(config_file)

    iso_name, iso_file = base.download_file(iso_info['url'], iso_info['local_path'])
    #     profile = '-'.join([iso_name, iso_info['arch']])

    result = base.checkisomd5(iso_file)
    if result is not True:
        sys.exit()

    # Mount iso to mounted_dir
    mounted_dir = base.mount_iso(iso_file, iso_info['mounted_root_dir'])

    if mounted_dir is None:
        sys.exit()

    task_id, profile = ch.import_image(iso_name, iso_info['arch'], mounted_dir)
    result = ch.get_status(task_id)
    if result:
        logger.info("Import image successfully.")
        for host, host_info in hosts_info.iteritems():
            create_kickstart(host_info, profile)
            ch.add_system(host_info, profile)

        for host, host_info in hosts_info.iteritems():
            if host_info['method'] == 'ssh':
                client_reinstall_ssh(host_info)
            if host_info['method'] == 'vmware':
                client_reinstall_vmware(host_info)

    # Unmount mounted dir
    base.umount_iso(mounted_dir)


def write_tmp_file(name, content):
    """
        Write temp file
    """
    ks_file = os.path.join('/tmp', name)
    with open(ks_file, 'w') as f:
        f.write(content)
    return ks_file


def deploy_hosts(request_result):
    host_infos = request_result['host']
    profile = request_result['profile']
    ks_name = request_result['ks']['name']
    ks_content = request_result['ks']['content']
    ks_file_path = write_tmp_file(ks_name, ks_content)
    ch = deploy_api.cobblerHandle()
    for host_info in host_infos:
        print host_info
        host_info['kickstart'] = ks_file_path
        print create_kickstart(host_info, profile)
        ch.add_system(host_info, profile)

        if host_info['deploy_method']['type'] == str(2):
            host_info['vmware_ip'] = host_info['deploy_method']['ip']
            host_info['vmware_user'] = host_info['deploy_method']['user']
            host_info['vmware_passwd'] = host_info['deploy_method']['password']
            client_reinstall_vmware(host_info)
        elif host_info['deploy_method']['type'] == str(1):
            client_reinstall_ssh(host_info)


def import_iso(url):
    ch = deploy_api.cobblerHandle()
    download_path = '/root'
    mounted_root_dir = '/mnt'
    arch = 'x86_64'
    iso_name, iso_file = base.download_file(url, download_path)
    #     profile = '-'.join([iso_name, iso_info['arch']])

    result = base.checkisomd5(iso_file)
    if result is not True:
        sys.exit()

    # Mount iso to mounted_dir
    mounted_dir = base.mount_iso(iso_file, mounted_root_dir)

    if mounted_dir is None:
        sys.exit()

    task_id, profile = ch.import_image(iso_name, arch, mounted_dir)
    result = ch.get_status(task_id)
    if result:
        logger.info("Import image successfully.")

    # Unmount mounted dir
    base.umount_iso(mounted_dir)
    return {'iso_name': iso_name, 'result': result}


def add_host_to_profile(config_file, host_name, profile):
    ch = deploy_api.cobblerHandle()
    hosts_info = common.get_hosts_info(config_file)
    for host, host_info in hosts_info.iteritems():
        if host == host_name:
            create_kickstart(host_info, profile)
            ch.add_system(host_info, profile)


def deploy_host(config_file, host_name):
    hosts_info = common.get_hosts_info(config_file)

    for host, host_info in hosts_info.iteritems():
        if host == host_name:
            if host_info['method'] == 'ssh':
                client_reinstall_ssh(host_info)
            if host_info['method'] == 'vmware':
                client_reinstall_vmware(host_info)


def remove_system(name):
    ch = deploy_api.cobblerHandle()
    ch.remove_system(name)


def remove_distro(name):
    ch = deploy_api.cobblerHandle()
    ch.remove_distro(name)


def get_all_distros():
    ch = deploy_api.cobblerHandle()
    all_distros = ch.get_all_distros()
    return all_distros


if __name__ == '__main__':
    run()


# deploy_()
