'''
Created on Nov 27, 2012

@author: wolf
'''
# !/usr/bin/env python

from distutils.core import setup
import distutils.command.install_data
import os
import string


def data_list():
    lists = list()

    for parent, dirs, files in os.walk('data/cobbler'):
        for file in files:
            path = os.path.join(parent, file)
            lists.append((string.replace(os.path.dirname(path), 'data', '/var/lib'), [path]))

    for parent, dirs, files in os.walk('conf'):
        for file in files:
            path = os.path.join(parent, file)
            lists.append((string.replace(os.path.dirname(path), 'conf', '/etc/contiudelivery'), [path]))

    data = [
        # ('/etc/contiudelivery/', ['conf/cobbler.conf', 'conf/deploy.conf', 'conf/logging.conf', 'conf/deploy.yaml',
        # 'conf/manager.ks', 'conf/slave.ks', 'conf/ns7.2.ks']),
        # ('/etc/contiudelivery/ovirt', ['conf/ovirt/ovirt-3.6.1-manager.ks', 'conf/ovirt/ovirt-3.6.1-node.ks']),
        ('/usr/local/bin', ['bin/ctdy-deploy', 'bin/ctdyDeployService']),
        ('/etc/cron.d', ['data/contiudelivery']),
        ('/usr/local/contiudelivery', ['data/autoctdy.sh']),
        ('/usr/local/contiudelivery/cobbler', ['conf/cobbler/dhcp.template', 'conf/cobbler/dnsmasq.template']),
        ('/usr/local/contiudelivery/ntp', ['data/ntp/ntp.conf']),
        ('/etc/systemd/system', ['data/ctdydeploy.service']),
    ]

    lists.extend(data)
    return lists


lists = data_list()

setup(name='contiudelivery-deploy',
      version='1.1',
      description='Python Distribution Utilities',
      author='Luo fei',
      author_email='luofei163@126.com',
      package_dir={'': 'src'},
      packages=['contiudelivery', 'contiudelivery.deploy'],

      data_files=lists,

      )
