# Kickstart file automatically generated by anaconda.

#version=DEVEL
nobody --enable
install
url --url="http://10.1.110.110/cblr/links/ovirt-license-x86_64"
#lang en_US.UTF-8
lang zh_CN
keyboard us
$SNIPPET('network_config')
rootpw  --iscrypted $6$ATHwHDpTWYAMB/nI$g5c.jpNNtIpjqJb3H39iadaEg7bdlf/xLgcSMs5JhkaKxChTK.Q20fnco8LJh.eNvjLlJLod0paVG1F8BZSnP0
firewall --disabled
authconfig --enableshadow --passalgo=sha512
selinux --disabled
timezone Asia/Shanghai
bootloader --location=mbr --driveorder=vda --append="crashkernel=auto rhgb quiet"
# The following is the partition information you requested
# Note that any partitions you deleted are not expressed
# here so unless you clear all partitions first, this is
# not guaranteed to work
zerombr
clearpart --all --initlabel
part /boot --fstype="ext4" --size=100
part swap --fstype="swap" --size=2048
part / --fstype="ext4" --grow --size=1

reboot

%packages
@core
@Virtualization Service and Management System
%end

%pre
/usr/sbin/parted -s /dev/vda mklabel msdos
$SNIPPET('pre_install_network_config')
%end

%post
configHostname()
{
echo "
NETWORKING=yes
ETC_HOSTNAME=
" > /mnt/sysimage/etc/sysconfig/network

echo "
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain
ETC_HOSTS
" > /mnt/sysimage/etc/hosts
}

configHostname

#Init Mail address
sed -i '$a set from=ctdy.dev@cs2c.com.cn' /etc/mail.rc
sed -i '$a set smtp=mail.cs2c.com.cn' /etc/mail.rc
sed -i '$a set smtp-auth-user=ctdy.dev@cs2c.com.cn' /etc/mail.rc
sed -i '$a set smtp-auth-password=qwer1234' /etc/mail.rc
sed -i '$a set smtp-auth=login' /etc/mail.rc

# Init SC and ovirt
cd /opt/systemcenter2.0/api/rest/config/
/etc/init.d/postgresql start
./db_init.sh  0  >> /var/log/sc_conifg.log  2>&1
./db_init.sh   1  >> /var/log/sc_conifg.log  2>&1
./create_pgsqldb.sh   >> /var/log/sc_conifg.log  2>&1
./create_scdb.sh  >> /var/log/sc_conifg.log  2>&1
./db_init.sh  2 >> /var/log/sc_conifg.log  2>&1
./systemcenterweb_configure.sh  10.1.82.34 8100 5432 0 1 0 > /var/log/sc_conifg.log 2>&1
./ovirt_env_check.sh   >> /var/log/sc_conifg.log 2>&1

#Init ovirt in rc.local
ovirt_stop="\ninit=1\nif [ @@init -ne 0 ]\nthen\n/etc/init.d/ovirt-engine stop"
echo -e "${ovirt_stop}" >> /etc/rc.d/rc.local

ovirt_install="/usr/bin/engine-setup --config=/opt/systemcenter2.0/api/rest/config/sc_ovirt_install >> /var/log/sc_conifg.log"
echo -e "${ovirt_install}" >> /etc/rc.d/rc.local

sc_restart="/etc/init.d/systemcenter restart"
echo -e "${sc_restart}" >> /etc/rc.d/rc.local

cmd_end="fi\nsed -i 's/init=1/init=0/g' /etc/rc.d/rc.local"
echo -e "${cmd_end}" >> /etc/rc.d/rc.local

#Replace @@ to $ in rc.local
sed -i 's/@@/$/g' /etc/rc.d/rc.local

$SNIPPET('post_install_network_config')
$yum_config_stanza
$SNIPPET('kickstart_done')


%end