#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
url --url="http://10.1.110.110/cblr/links/mlk-x86_64"
# Use graphical install
graphical
# Run the Setup Agent on first boot
firstboot --disable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=cn --xlayouts='cn'
# System language
lang zh_CN.UTF-8

# Network information
network  --bootproto=static --device=eth0 --gateway=10.1.110.254 --ip=10.1.111.242 --nameserver=211.167.230.100 --netmask=255.255.248.0 --ipv6=auto --activate
network  --hostname=ovirtManager

# Root password
rootpw --iscrypted $6$RCVG0e0kZim5FDU8$q.FDH1eNR.qHh.ZU9GSAT5zOEPLmQkukmb7OU16oXIFoT3GscpaAYPPn6cNaNlpKNEuvUiveZo2CF9.YWoeXc/
# System timezone
timezone Asia/Shanghai --isUtc
# X Window System configuration information
xconfig  --startxonboot
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=vda

reboot

%packages
@^virtualization-server-and-management-system
@base
@core
@desktop-debugging
@dial-up
@fonts
@gnome-desktop
@guest-agents
@guest-desktop-agents
@input-methods
@internet-browser
@multimedia
@nkev-manager
@nkev-node
@print-client
@x11
kexec-tools

%end

%post
$SNIPPET('post_install_network_config')
$yum_config_stanza
$SNIPPET('kickstart_done')
%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end
