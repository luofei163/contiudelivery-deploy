#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
url --url="http://10.1.110.110/cblr/links/mlk-x86_64"
# Use graphical install
graphical
# Run the Setup Agent on first boot
firstboot --disable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=cn --xlayouts='cn'
# System language
lang zh_CN.UTF-8

# Network information
network  --bootproto=static --device=eth0 --gateway=10.1.110.254 --ip=10.1.111.243 --nameserver=211.167.230.100 --netmask=255.255.248.0 --ipv6=auto --activate
network  --hostname=node243

# Root password
rootpw --iscrypted $6$/rTA1OzkNXiQdI7G$zR.1Zsu0U0UQqSPvBRbT2rp.nF3Ku0ZLPiGpBSmUw/70UYUzv.KSQNBj9jlhbX1t1td0uU7kAOdDNV7.4paXy1
# System timezone
timezone Asia/Shanghai --isUtc
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=vda

reboot

%packages
@^virtualization-server
@core
@nkev-node
kexec-tools

%end

%post
$SNIPPET('post_install_network_config')
$yum_config_stanza
$SNIPPET('kickstart_done')
%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end
