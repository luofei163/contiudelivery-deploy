#version=DEVEL
# Firewall configuration
firewall --enabled
# Install OS instead of upgrade
install
# Use network installation
#cdrom
url --url="http://192.168.3.2:80/cobbler/ks_mirror/nkscloud21-x86_64"
# Root password
rootpw --iscrypted $1$UaaoFPT6$H6tVmOSWlYIoidVVq.4t6.
# System authorization information
auth  --useshadow  --passalgo=md5
# Use text mode install
text
firstboot --disable
# System keyboard
keyboard us
# System language
lang en_US
# SELinux configuration
selinux --enforcing
# Installation logging level
logging --level=info
# Reboot after installation
#reboot --eject
# System timezone
timezone  Asia/Shanghai
# System bootloader configuration
bootloader --location=mbr
# Partition clearing information
clearpart --all --initlabel 
# Disk partitioning information
part swap --fstype="swap" --size=8000
part /boot --fstype="ext4" --size=200
part / --fstype="ext4" --size=40000
part /neofs/data/sda5 --fstype="xfs" --grow --size=1

%post --nochroot
#Config hostname and host full name
configHostname()
{
echo "
NETWORKING=yes
ETC_HOSTNAME=
" > /mnt/sysimage/etc/sysconfig/network

echo "
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain
ETC_HOSTS
" > /mnt/sysimage/etc/hosts
}



configRclocal()
{
echo "
ntpdate $cobbler_server_ip
puppet agent --server=$serverhostname
" >> /mnt/sysimage/etc/rc.local
}

configHostname
configRclocal

#pxe install once
$SNIPPET('kickstart_done')

reboot




