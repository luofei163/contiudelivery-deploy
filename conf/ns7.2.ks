#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
#cdrom
url --url="http://192.168.3.2:80/cobbler/ks_mirror/nkscloud21-x86_64"
# Use graphical install
graphical
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=cn --xlayouts='cn'
# System language
lang zh_CN.UTF-8

# Network information
network  --bootproto=dhcp --ipv6=auto

# Root password
rootpw --iscrypted $6$plDk4G3EET1Bqy0p$jFvum3hLb.F1eu35KXhzKZUeXIe0Q2FuhsogUX1h3j/y7JessmpIhBTb6gHllyaUx8p61sDXMEenfaZigsbv./
# System timezone
timezone Asia/Shanghai --isUtc
user --groups=wheel --name=wolf --password=$6$MbnmmfUwiVOQX2YE$uyJLGBacfW2K/pgpPBu8kXRCno5f7VpCgZPZjyGJfwSTILJLyp.hRIrrvY7DGRNKm2oMS0ZsfHChfPJcjxWTA/ --iscrypted --gecos="wolf"
# X Window System configuration information
xconfig  --startxonboot
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr 
# Partition clearing information
clearpart --all --initlabel
part /boot --fstype xfs --size=200
part swap --fstype="swap" --size=2048
part / --fstype xfs --grow --size=1

%packages
@^graphical-server-environment
@backup-server
@base
@compat-libraries
@core
@desktop-debugging
@development
@dial-up
@dns-server
@file-server
@fonts
@ftp-server
@gnome-desktop
@guest-agents
@guest-desktop-agents
@hardware-monitoring
@identity-management-server
@infiniband
@input-methods
@internet-browser
@java-platform
@kde-desktop
@large-systems
@load-balancer
@mail-server
@mainframe-access
@mariadb
@multimedia
@network-file-system-client
@performance
@postgresql
@print-client
@print-server
@remote-system-management
@security-tools
@smart-card
@virtualization-client
@virtualization-hypervisor
@virtualization-tools
@x11
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end



%post --nochroot

#Config hostname and host full name
configHostname()
{
echo "
NETWORKING=yes
ETC_HOSTNAME=
" > /mnt/sysimage/etc/sysconfig/network

echo "
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain
ETC_HOSTS
" > /mnt/sysimage/etc/hosts
}


#Config host IP address and so on
configIp()
{
echo "
DEVICE=eth0
BOOTPROTO=static
NM_CONTROLLED=yes
ONBOOT=yes
TYPE=Ethernet
IPADDR=192.168.3.66
NETMASK=255.255.252.0
GATEWAY=192.168.3.1
DNS1=8.8.8.8
IPV6INIT=no
USERCTL=no
" > /mnt/sysimage/etc/sysconfig/network-scripts/ifcfg-eth0
}


configYum()
{
rm -rf /mnt/sysimage/etc/yum.repos.d/*.repo
echo "
[koji_ns7.2]
name=koji_ns7.2
baseurl=http://10.1.122.102/kojifiles/repos/ns7.2-build/latest/x86_64/
gpgcheck=0
enabled=1		

" > /mnt/sysimage/etc/yum.repos.d/cs2c.repo
}
configYum
configHostname
#configIp

#pxe install once
$SNIPPET('kickstart_done')

reboot
%end
